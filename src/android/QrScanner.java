package cordova.plugin.qrscanner;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaActivity;

import android.provider.MediaStore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
 import android.content.Intent;
import android.widget.ImageView; 
import android.graphics.Bitmap;
import android.content.Context;
import android.widget.Toast;
import com.google.zxing.client.android.Intents;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import org.apache.cordova.PluginResult;


/**
 * This class echoes a string called from JavaScript.
 */
public class QrScanner extends CordovaPlugin {
     private CallbackContext onNewIntentCallbackContext = null;

		private static final int pic_id = 123;
		ImageView click_image_id; 


    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("coolMethod")) {
            String message = args.getString(0);
            this.coolMethod(message, callbackContext);
            return true;
        }
        return false;
    }

    private void coolMethod(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
					        // get activity
		CordovaActivity myActivity = ((CordovaActivity) cordova.getActivity());
				this.cordova.setActivityResultCallback((CordovaPlugin)this);


		IntentIntegrator integrator = new IntentIntegrator((CordovaActivity) cordova.getActivity());
		integrator.initiateScan();
           this.onNewIntentCallbackContext = callbackContext;
 
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }
	
	
	
    @Override
 	public void onActivityResult(int requestCode, int resultCode, Intent intent){
		IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
		if (scanResult != null) {
			this.onNewIntentCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK,scanResult.getContents()));
		}
	}
		
		
		
 

}
